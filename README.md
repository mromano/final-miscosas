# final-miscosas


# Entrega practica
## Datos
* Nombre: Marina Romano Barrena
* Titulación: Doble Grado en Ingeniería en Sistemas de Telecomunicación + ADE
* Despliegue (url): http://mromano.pythonanywhere.com
* Video básico (url): https://youtu.be/ag903uzSgMU
* Video parte opcional (url): https://youtu.be/iNaiG4L2cjA

## Cuenta Admin Site
* superuser/Miscosas
## Cuentas usuarios
* marina1/Miscosas1
* marina2/Miscosas2

## Resumen parte obligatoria
La aplicación "MisCosas" se trata de un gestor de contenidos que utiliza información de dos tipos de alimentadores: canales de Youtube y revisiones de páginas de Wikipedia. Los alimentadores son elegidos mediante un formulario en la página principal: se introduce tanto el id del canal como el título de la página de wikipedia, con esa información se crea la url del documento XML asociado, se parsean los campos interesantes para la aplicación y se guardan en la base de datos. En todas las páginas de la aplicación se permite a un individuo identificarse como usuario, y así poder votar y comentar los items disponibles. Además, se proporciona un menú desde el cual se puede acceder a las distintas páginas de la aplicación (página de usuarios, página de alimentadores y página de información) para ver toda la información disponible y acceder a todas las funcionalidades de la aplicación.
## Lista partes opcionales
* Nombre parte: Inclusión de un favicon del sitio.
* Nombre parte: Visualización de cualquier página en formato JSON y/o XML, de forma similar a como se ha indicado para la página principal.
* Nombre parte: Atención al idioma indicado por el navegador. El idioma de la interfaz de usuario de la aplicación tendrá en cuenta lo que especifique el navegador.