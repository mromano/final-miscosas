from django.test import TestCase
from . import views
from .models import Feed, Video, Revision
from .ytparser import YTChannel
from .wkparser import WKPage


import urllib.request

# Create your tests here.

xml = ""
class TestYtParser(TestCase):
    def setUp(self):
        self.url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'

        self.feedTitle = "CursosWeb"
        self.feedLink = "https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg"
        self.feedType = "yt"
        self.feedSelected = True

        self.title = "Presentación de la práctica final (dudas de alumnos 2)"
        self.link = "https://www.youtube.com/watch?v=HZOodD84MR8"
        self.videoId = "HZOodD84MR8"
        self.description = "Grabación de algunas preguntas y dudas de alumnos sobre la práctica final, y las respuestas y comentarios de los profesores al respecto."

        xmlStream = urllib.request.urlopen(self.url)
        channel = YTChannel(xmlStream)

        #canal
        try:
            c = Feed.objects.get(title=channel.canal()['nombreCanal'])
        except Feed.DoesNotExist:
            canal = Feed(title=channel.canal()['nombreCanal'], link=channel.canal()['urlCanal'],
                        type='yt', selected=True)
            canal.save()
        #videos del canal
        for video in channel.videos():
            #sólo lo meto en la base de datos si no existe ya
            try:
                c = Video.objects.get(title=video['title'])
            except Video.DoesNotExist:

                video = Video(title=video['title'], identificador=str(hash(video['title'])),
                        description=video['description'], link=video['link'], videoId=video['videoId'],
                        feed=Feed.objects.get(title=channel.canal()['nombreCanal']))
                video.save()


    def testFirst(self):
        videos = Video.objects.all()
        video = Video.objects.get(videoId = "HZOodD84MR8")

        self.assertEqual(len(videos), 15)

        self.assertEqual(video.title, self.title)
        self.assertEqual(video.link, self.link)
        self.assertEqual(video.videoId, self.videoId)
        self.assertEqual(video.description, self.description)

    def testSecond(self):
        feed = Feed.objects.get(title = "CursosWeb")

        self.assertEqual(feed.title, self.feedTitle)
        self.assertEqual(feed.link, self.feedLink)
        self.assertEqual(feed.type, self.feedType)
        self.assertEqual(feed.selected, self.feedSelected)

class TestWkParser(TestCase):
    def setUp(self):
        self.page="Fuenlabrada"
        self.url = 'https://en.wikipedia.org/w/index.php?title=' + self.page + '&action=history&feed=rss'

        self.feedTitle = "Fuenlabrada"
        self.feedLink = "https://en.wikipedia.org/w/index.php?title=Fuenlabrada"
        self.feedType = "wk"
        self.feedSelected = True

        self.title = "Asqueladd: /* Education */"
        self.link = "https://en.wikipedia.org/w/index.php?title=Fuenlabrada&diff=946948335&oldid=prev"
        self.author = "Asqueladd"
        self.date = "Mon, 23 Mar 2020 10:44:09 GMT"

        xmlStream = urllib.request.urlopen(self.url)
        wiki = WKPage(xmlStream)

        #pagina
        try:
            c = Feed.objects.get(title=self.page)
        except Feed.DoesNotExist:
            pagina = Feed(title=self.page, link="https://en.wikipedia.org/w/index.php?title=" + self.page,
                        type = 'wk', selected=True)
            pagina.save()
        #revisiones de la página
        for revision in wiki.revisions():
            try:
                c = Revision.objects.get(title=revision['title'])
            except Revision.DoesNotExist:
                revision = Revision(title=revision['title'], identificador=str(hash(revision['title'])),
                                    author=revision['author'], link=revision['link'], date=revision['date'],
                                    feed=Feed.objects.get(title=self.page))
                revision.save()

    def testFirst(self):
        revisions = Revision.objects.all()
        revision = Revision.objects.get(title="Asqueladd: /* Education */")

        self.assertEqual(len(revisions), 10)

        self.assertEqual(revision.title, self.title)
        self.assertEqual(revision.link, self.link)
        self.assertEqual(revision.author, self.author)
        self.assertEqual(revision.date, self.date)

    def testSecond(self):
        feed = Feed.objects.get(title = "Fuenlabrada")

        self.assertEqual(feed.title, self.feedTitle)
        self.assertEqual(feed.link, self.feedLink)
        self.assertEqual(feed.type, self.feedType)
        self.assertEqual(feed.selected, self.feedSelected)


class TestViewsMain(TestCase):
    def setUp(self):
        self.myfeed = "CursosWeb"
        self.myitem = str(hash("Presentación de la práctica final (dudas de alumnos 2)"))
        self.myuser = "marina1"

    # GET /
    def testGet1(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testGet1_func(self):
        response = self.client.get('/')
        self.assertEqual(response.resolver_match.func, views.homepage)

    # POST / (login)
    def testPost1(self):
        response = self.client.post('/', {'action': 'Login',
                                            'username': "marina1",
                                            'password': "Miscosas1"})
        self.assertEqual(response.status_code, 200)

    # GET /feeds
    def testGet2(self):
        response = self.client.get('/feeds/')
        self.assertEqual(response.status_code, 200)

    # GET /feeds/<feed>
    def testGet3(self):
        response = self.client.get('/feeds/' + self.myfeed)
        self.assertEqual(response.status_code, 301)

    # POST /feeds/<feed> (Seleccionar)
    def testPost2(self):
        response = self.client.post('/feeds/' + self.myfeed, {'action': 'Seleccionar'})
        self.assertEqual(response.status_code, 301)

    # GET /feeds/<feed>/<id item>
    def testGet4(self):
        response = self.client.post('/feeds/' + self.myfeed + "/" + self.myitem)
        self.assertEqual(response.status_code, 301)

    # POST /feeds/<feed>/<id item> (Seleccionar)
    def testPost3(self):
        response = self.client.post('/feeds/' + self.myfeed + "/" +
                                    self.myitem, {'action': 'Like'})
        self.assertEqual(response.status_code, 301)

    #GET /users
    def testGet5(self):
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)

    #GET /users/<user>
    def testGet6(self):
        response = self.client.get('/users/' + self.myuser)
        self.assertEqual(response.status_code, 301)

    #GET /info
    def testGet7(self):
        response = self.client.get('/info/')
        self.assertEqual(response.status_code, 200)
