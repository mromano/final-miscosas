from urllib.request import urlopen
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string


class WKHandler(ContentHandler):

    def __init__ (self):

        self.inItem = False
        self.inContent = False
        self.content = ""

        self.title = ""
        self.link = ""
        self.author = ""
        self.date = ""

        self.revisions = []


    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':             #revision
                self.inContent = True
            elif name == 'link':            #link (ya)
                self.inContent = True
            elif name == 'dc:creator':      #author
                self.inContent = True
            elif name == 'pubDate':         #date
                self.inContent = True

    def endElement (self, name):
        global revisions

        if name == 'item':
            self.inItem = False
            self.revisions.append({'link': self.link,
                                'title': self.title,
                                'author': self.author,
                                'date': self.date})
            #print(self.revisions)

        elif self.inItem:
            if name == 'title':
                self.title = self.content
            elif name == 'link':
                self.link = self.content
            elif name == 'dc:creator':
                self.author = self.content
            elif name == 'pubDate':
                self.date = self.content

            #lo pongo al final porque aparecía en todas
            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


class WKPage:
    #esto es siempre igual
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WKHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def revisions (self):
        return self.handler.revisions



# Load parser and driver
#Parser = make_parser()
#Parser.setContentHandler(WKHandler())

#if __name__ == "__main__":

    #if len(sys.argv)<2:
        #print("Usage: python xml-parser-youtube.py <document>")
        #print()
        #print(" <document>: file name of the document to parse")
        #sys.exit(1)

    # Ready, set, go!
    #xmlFile = urlopen(sys.argv[1])

    #Parser.parse(xmlFile)
