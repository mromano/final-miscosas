from django.contrib import admin
from .models import Video, Feed, Revision, UserMisCosas, Comment, Rating

# Register your models here.
admin.site.register(Video)
admin.site.register(Revision)
admin.site.register(Feed)
admin.site.register(UserMisCosas)
admin.site.register(Comment)
admin.site.register(Rating)
