from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

import urllib.request

from .ytparser import YTChannel
from .wkparser import WKPage
from .models import Video, Feed, Revision, UserMisCosas, Comment, Rating

from django.views.decorators.csrf import csrf_protect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

import datetime

from itertools import chain
from operator import attrgetter


def change_style(request):
    if request.user.is_authenticated:
        #saco el usuario y devuelvo sus preferencias
        user = UserMisCosas.objects.get(username=request.user.username)
        estilo = user.style
        tamaño = user.size
        if estilo == "Ligero":
            background = "white"
            color = "black"
        elif estilo == "Oscuro":
            background = "black"
            color = "white"
        if tamaño == "Pequeña":
            size = "0.9vw"
        elif tamaño == "Normal":
            size = "1.25vw"
        elif tamaño == "Grande":
            size = "1.7vw"
    else:
        #devuelvo preferencias default
        background = "white"
        color = "black"
        size = "1.25vw"

    return background, color, size


def loguear(request, username, password):
    #busco si ya existe el usuario
    user = authenticate(username=username, password=password)
    if user != None:
        #si ya existe, le logueo
        login(request, user)
        #preparo el contexto y rendeo
        message = "Hola, " + username + ". Click aquí para salir:"
        authenticated = True
        return authenticated, message
    else:
        try:
            #si sí que existe, pero la contraseña está mal
            #voy a la misma página (+ mensaje de error)
            user = User.objects.get(username=username)
            #preparo el contexto y rendeo
            authenticated = False
            message = "Error al introducir contraseña. Prueba de nuevo:"
            return authenticated, message
        except:
            #si el usuario no existe:
            #le añado a la base de datos (foto y nº feeds es default)
            newUserDatabase = UserMisCosas(username=username)
            newUserDatabase.save()
            #le creo y le logueo
            newUser = User.objects.create_user(username=username, password=password)
            newUser.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            #preparo el contexto y rendeo
            authenticated = True
            message = "Nuevo usuario registrado: " + username + ". Click aquí para salir:"
            return authenticated, message

@csrf_protect
def homepage(request):
    if request.method == "POST":
        action = request.POST['action']

        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)
            #formulario
            c = Video.objects.all()
            a = Revision.objects.all()
            b = Feed.objects.all()
            last5items = []
            if request.user.is_authenticated:
                username = request.user.username
                #últimos 5 items votados por el usuario
                #primero saco los id del objeto Rating
                last5ratings = []
                voted_by_user = Rating.objects.filter(user=request.user)
                if len(voted_by_user) <= 5:
                    last5ratings = voted_by_user
                else:
                    length = len(voted_by_user)
                    last5ratings = voted_by_user[(length-5):(length)]

                #una vez tengo la lista con los objetos, saco el id con un for
                #y me guardo en una lista los objetos asociados
                last5items = []
                for rating in last5ratings:
                    id = rating.identificador
                    feedType = rating.feedType
                    if feedType == "yt":
                        video = Video.objects.get(identificador=id)
                        last5items.append(video)
                    if feedType == "wk":
                        revision = Revision.objects.get(identificador=id)
                        last5items.append(revision)

            urls = []
            for feed in b:
                urls.append(feed.link)

            a_puntuated = Video.objects.filter(puntuated=True)
            c_puntuated = Revision.objects.filter(puntuated=True)
            top10 = sorted(chain(a_puntuated, c_puntuated), key=attrgetter('punctuation'), reverse=True)[0:9]

            [background, color, size] = change_style(request)

            context = {'authenticated': authenticated, 'message': message, 'feed_list': b,
                        'video_list': c, 'wiki_list': a, 'urls': urls, 'top10': top10,
                        'last5items': last5items, 'homepage': True,
                        'background': background, 'color': color, 'size': size}
            return render(request,'miscosas/homepage.html', context)

        elif action == "Like":
            title = request.POST['title']
            feedType = request.POST['feedType']
            #obligatoriamente hay un voto para ese item de el mismo usuario
            #(ya que están en el apartado de "últimos items votados por el usuario")

            #saco el id del item al que se refiere esa puntuación
            if feedType == "yt":
                a = Video.objects.get(title=title)
            if feedType == "wk":
                a = Revision.objects.get(title=title)

            id = a.identificador

            #cambio el rating del usuario para ese item si es distinto
            #y actualizo los campos del item
            #(es imposible que salga Rating.DoesNotExist)
            user = request.user
            b = Rating.objects.get(user=user, identificador=id)
            if b.punctuation == "Dislike":
                #cambio el like por dislike
                b.punctuation = "Like"
                b.save()

                a.likes = a.likes + 1
                a.punctuation = a.punctuation + 1
                a.instantLiked = True
                a.save()

        elif action == "Dislike":
            title = request.POST['title']
            feedType = request.POST['feedType']
            #obligatoriamente hay un voto para ese item de el mismo usuario
            #(ya que están en el apartado de "últimos items votados por el usuario")

            #saco el id del item al que se refiere esa puntuación
            #y actualizo sus campos
            if feedType == "yt":
                a = Video.objects.get(title=title)
            if feedType == "wk":
                a = Revision.objects.get(title=title)

            id = a.identificador

            #cambio el rating del usuario para ese item si es distinto
            #y actualizo los campos del item
            #(es imposible que salga Rating.DoesNotExist)
            user = request.user
            b = Rating.objects.get(user=user, identificador=id)
            if b.punctuation == "Like":
                #cambio el like por dislike
                b.punctuation = "Dislike"
                b.save()

                a.dislikes = a.dislikes + 1
                a.punctuation = a.punctuation - 1
                a.instantLiked = False
                a.save()


        elif action == "Seleccionar":
            #lo marco como seleccionado (solo si el usuario está autenticado)

            #redirecciono a la página del alimentador
            chosen = request.POST['title']
            d = Feed.objects.get(title=chosen)
            d.selected = True
            d.save()

            return redirect('/feeds/'+ d.title)

        elif action == "Deseleccionar":
            #lo marco como deseleccionado
            #en el template (homepage.html) me encargo de solo pintar los seleccionados
            chosen = request.POST['title']
            d = Feed.objects.get(title=chosen)
            d.selected = False
            d.save()


        elif action == "Enviar ID":
        #recojo info para youtube
            id = request.POST['id']
            #parseo y guardo en la base de datos
            url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
                  + id
            xmlStream = urllib.request.urlopen(url)
            channel = YTChannel(xmlStream)
            #canal
            try:
                c = Feed.objects.get(title=channel.canal()['nombreCanal'])
            except Feed.DoesNotExist:
                canal = Feed(title=channel.canal()['nombreCanal'], link=channel.canal()['urlCanal'],
                            type='yt', selected=True)
                canal.save()
            #videos del canal
            for video in channel.videos():
                #sólo lo meto en la base de datos si no existe ya
                try:
                    c = Video.objects.get(title=video['title'])
                except Video.DoesNotExist:

                    video = Video(title=video['title'], identificador=str(hash(video['title'])),
                            description=video['description'], link=video['link'], videoId=video['videoId'],
                            feed=Feed.objects.get(title=channel.canal()['nombreCanal']))
                    video.save()
            #redirecciono a la página del nuevo alimentador
            return redirect("/feeds/" + channel.canal()['nombreCanal'])

        elif action == "Enviar Página":
            #recojo info para wikipedia
            page = request.POST['pagina']
            #parseo y guardo en la base de datos
            url = "https://en.wikipedia.org/w/index.php?title=" + page \
                    + "&action=history&feed=rss"
            xmlStream = urllib.request.urlopen(url)
            wiki = WKPage(xmlStream)
            #pagina
            try:
                c = Feed.objects.get(title=page)
            except Feed.DoesNotExist:
                pagina = Feed(title=page, link="https://en.wikipedia.org/w/index.php?title=" + page,
                            type = 'wk', selected=True)
                pagina.save()
            #revisiones de la página
            for revision in wiki.revisions():
                try:
                    c = Revision.objects.get(title=revision['title'])
                except Revision.DoesNotExist:
                    revision = Revision(title=revision['title'], identificador=str(hash(revision['title'])),
                                        author=revision['author'], link=revision['link'], date=revision['date'],
                                        feed=Feed.objects.get(title=page))
                    revision.save()
            #redirecciono a la página del nuevo alimentador
            return redirect("/feeds/" + page)
    #formulario
    c = Video.objects.all()
    a = Revision.objects.all()
    b = Feed.objects.all()
    last5items = []
    if request.user.is_authenticated:
        authenticated = True
        username = request.user.username
        message = "Hola, " + username + ". Click aquí para salir:"

        #últimos 5 items votados por el usuario
        #primero saco los id del objeto Rating
        last5ratings = []
        voted_by_user = Rating.objects.filter(user=request.user)
        if len(voted_by_user) <= 5:
            last5ratings = voted_by_user
        else:
            length = len(voted_by_user)
            last5ratings = voted_by_user[(length-5):(length)]

        #una vez tengo la lista con los objetos, saco el id con un for
        #y me guardo en una lista los objetos asociados
        last5items = []
        for rating in last5ratings:
            id = rating.identificador
            feedType = rating.feedType
            if feedType == "yt":
                video = Video.objects.get(identificador=id)
                last5items.append(video)
            if feedType == "wk":
                revision = Revision.objects.get(identificador=id)
                last5items.append(revision)
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    urls = []
    for feed in b:
        urls.append(feed.link)

    a_puntuated = Video.objects.filter(puntuated=True)
    c_puntuated = Revision.objects.filter(puntuated=True)
    top10 = sorted(chain(a_puntuated, c_puntuated), key=attrgetter('punctuation'), reverse=True)[0:9]

    [background, color, size] = change_style(request)

    context = {'authenticated': authenticated, 'message': message, 'feed_list': b,
                'video_list': c, 'wiki_list': a, 'urls': urls, 'top10': top10,
                'last5items': last5items, 'homepage': True,
                'background': background, 'color': color, 'size': size}
    return render(request,'miscosas/homepage.html', context)

def userspage(request):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)

            users = UserMisCosas.objects.all()
            c = Feed.objects.all()
            urls = []
            for feed in c:
                urls.append(feed.link)

            [background, color, size] = change_style(request)

            context = {'authenticated': authenticated, 'message': message, 'users': users,
                        'urls': urls, 'feed_list': c, 'userspage': True,
                        'background': background, 'color': color, 'size': size}
            return render(request, 'miscosas/userspage.html', context)

    users = UserMisCosas.objects.all()
    user = request.user
    if user.is_authenticated:
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    c = Feed.objects.all()
    urls = []
    for feed in c:
        urls.append(feed.link)

    [background, color, size] = change_style(request)

    context = {'authenticated': authenticated, 'message': message, 'users': users,
                'urls': urls, 'feed_list': c, 'userspage': True,
                'background': background, 'color': color, 'size': size}
    return render(request, 'miscosas/userspage.html', context)

def userpage(request, id):
    myuser = UserMisCosas.objects.get(username=id)
    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)
            #items votados y comentados por el user con username=id
            itemsvoted = Rating.objects.filter(username=id)
            itemscommented = Comment.objects.filter(username=id)

            user = request.user
            if user.is_authenticated:
                if user.username == id:
                    authenticated_as_me = True
                else:
                    authenticated_as_me = False
            else:
                authenticated_as_me = False

            c = Feed.objects.all()

            [background, color, size] = change_style(request)

            context = {'authenticated': authenticated, 'authenticated_as_me': authenticated_as_me,
                        'message': message, 'user': myuser, 'feed_list': c, 'itemsvoted': itemsvoted,
                        'itemscommented': itemscommented, 'background': background, 'color': color, 'size': size}
            return render(request, 'miscosas/userpage.html', context)

        if action == "Enviar Foto":
            newphoto = request.POST['photo']
            #cambio la foto de mi usuario
            myuser.photo = newphoto
            myuser.save()

        if action == "Enviar Tamaño":
            #cambio las preferencias del usuario
            tamaño = request.POST['Tamaño']
            if tamaño == "Pequeña":
                myuser.size = "Pequeña"
                myuser.save()
            if tamaño == "Normal":
                myuser.size = "Normal"
                myuser.save()
            if tamaño == "Grande":
                myuser.size = "Grande"
                myuser.save()

        if action == "Enviar Estilo":
            #cambio las preferencias del usuario
            style = request.POST['Estilo']
            if style == "Ligero":
                myuser.style = "Ligero"
                myuser.save()
            if style == "Oscuro":
                myuser.style = "Oscuro"
                myuser.save()

    #items votados y comentados por el user con username=id
    itemsvoted = Rating.objects.filter(username=id)
    itemscommented = Comment.objects.filter(username=id)

    user = request.user
    if user.is_authenticated:
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
        if user.username == id:
            authenticated_as_me = True
        else:
            authenticated_as_me = False
    else:
        authenticated = False
        authenticated_as_me = False
        message = "Sesión de invitado. Registrarse:"

    c = Feed.objects.all()

    [background, color, size] = change_style(request)

    context = {'authenticated': authenticated, 'authenticated_as_me': authenticated_as_me,
                'message': message, 'user': myuser, 'feed_list': c, 'itemsvoted': itemsvoted,
                'itemscommented': itemscommented, 'background': background, 'color': color, 'size': size}
    return render(request, 'miscosas/userpage.html', context)

def feedspage(request):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)

            c = Feed.objects.all()
            urls = []
            for feed in c:
                urls.append(feed.link)

            [background, color, size] = change_style(request)

            context = {'authenticated': authenticated, 'message': message, 'feed_list': c, 'urls': urls,
                        'feedspage': True, 'background': background, 'color': color, 'size': size}
            return render(request, 'miscosas/feedspage.html', context)

    c = Feed.objects.all()
    user = request.user
    if user.is_authenticated:
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    urls = []
    for feed in c:
        urls.append(feed.link)

    [background, color, size] = change_style(request)

    context = {'authenticated': authenticated, 'message': message, 'feed_list': c, 'urls': urls,
                'feedspage': True, 'background': background, 'color': color, 'size': size}
    return render(request, 'miscosas/feedspage.html', context)

def feedpage(request, id):
    #datos del alimentador (detallado)
    #botón para elegir o dejar de elegir
    #no estaba elegido --> aparece en la página principal (selected = true)
    #estaba elegido --> desaparece de la página principal (selected = false)
    #items del alimentador (resumido) + enlace a la página del item
    a = Feed.objects.get(title=id)

    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)


            feedType = a.type
            if feedType == 'yt':
                #saco item Video
                b = Video.objects.filter(feed=a)
            elif feedType == 'wk':
                #saco item Revision
                b = Revision.objects.filter(feed=a)
            else:
                b = ""

            selected = a.selected

            c = Feed.objects.all()
            urls = []
            for feed in c:
                urls.append(feed.link)

            [background, color, size] = change_style(request)

            context = {'selected': selected, 'authenticated': authenticated, 'message': message,
                        'feed': a, 'items': b, 'urls': urls, 'feed_list': c,
                        'background': background, 'color': color, 'size': size}
            return render(request, 'miscosas/feedpage.html', context)

        elif action == "Seleccionar":
            a.selected = True
            a.save()

        elif action == "Deseleccionar":
            a.selected = False
            a.save()

    user = request.user
    if user.is_authenticated:
        username = request.user.username
        user = UserMisCosas.objects.get(username=username)
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    feedType = a.type
    if feedType == 'yt':
        #saco item Video
        b = Video.objects.filter(feed=a)
    elif feedType == 'wk':
        #saco item Revision
        b = Revision.objects.filter(feed=a)
    else:
        b = ""

    selected = a.selected

    c = Feed.objects.all()
    urls = []
    for feed in c:
        urls.append(feed.link)

    [background, color, size] = change_style(request)

    context = {'selected': selected, 'authenticated': authenticated, 'message': message,
                'feed': a, 'items': b, 'urls': urls, 'feed_list': c,
                'background': background, 'color': color, 'size': size}
    return render(request, 'miscosas/feedpage.html', context)

def itempage(request, id1, id2):
    a = Feed.objects.get(title=id1)
    feedType = a.type

    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)

            if feedType == 'yt':
                #saco item Video
                b = Video.objects.get(identificador=id2)
                c = Comment.objects.filter(identificador=id2)

                d = Feed.objects.all()
                urls = []
                for feed in d:
                    urls.append(feed.link)

                [background, color, size] = change_style(request)

                context = {'authenticated': authenticated, 'message': message, 'comments': c,
                            'feedType': feedType,'feed': a, 'item': b, 'urls': urls, 'feed_list': d,
                            'background': background, 'color': color, 'size': size}
                return render(request, 'miscosas/itempageYt.html', context)

            elif feedType == 'wk':
                #saco item Revision
                b = Revision.objects.get(identificador=id2)
                c = Comment.objects.filter(identificador=id2)

                d = Feed.objects.all()
                urls = []
                for feed in d:
                    urls.append(feed.link)

                [background, color, size] = change_style(request)

                context = {'authenticated': authenticated, 'message': message, 'comments': c,
                            'feedType': feedType, 'feed': a, 'item': b, 'urls': urls, 'feed_list': d,
                            'background': background, 'color': color, 'size': size}
                return render(request, 'miscosas/itempageWk.html', context)
            else:
                b = ""


        elif action == "Like":
            #busco si hay un voto para ese item y de el mismo usuario
            user = request.user
            username = user.username
            try:
                #si sí que existe, tengo que ver cuál es
                #(si es distinto, lo cambio --> si no, no hago nada)
                c = Rating.objects.get(identificador=id2, user=user)

                if c.punctuation == "Dislike":
                    c.punctuation = "Like"
                    c.save()

                    if feedType == "yt":
                        a = Video.objects.get(identificador=id2)
                    if feedType == "wk":
                        a = Revision.objects.get(identificador=id2)

                    a.punctuation = a.punctuation + 1
                    a.likes = a.likes + 1
                    a.instantLiked = True
                    a.save()

            except Rating.DoesNotExist:
                #si no existe, lo creo
                myuser = UserMisCosas.objects.get(username=username)
                myuser.itemsvoted = myuser.itemsvoted + 1
                myuser.save()

                if feedType == "yt":
                    a = Video.objects.get(identificador=id2)
                if feedType == "wk":
                    a = Revision.objects.get(identificador=id2)

                a.punctuation = a.punctuation + 1
                a.likes = a.likes + 1
                a.instantLiked = True
                a.puntuated = True
                a.save()

                title = a.title
                link = a.link
                rating = Rating(punctuation="Like", user=user, username=username,
                        identificador=id2, feedType=feedType, title=title, link=link)
                rating.save()


        elif action == "Dislike":
            #busco si hay un voto para ese item y de el mismo usuario
            user = request.user
            username = user.username
            try:
                #si sí que existe, tengo que ver cuál es
                #(si es distinto, lo cambio --> si no, no hago nada)
                c = Rating.objects.get(identificador=id2, user=user)
                #print(c)
                #print(c.punctuation)
                if c.punctuation == "Like":
                    c.punctuation = "Dislike"
                    c.save()

                    if feedType == "yt":
                        a = Video.objects.get(identificador=id2)
                        a.punctuation = a.punctuation - 1
                        a.dislikes = a.dislikes + 1
                        a.instantLiked = False
                        a.save()
                    if feedType == "wk":
                        a = Revision.objects.get(identificador=id2)
                        a.punctuation = a.punctuation - 1
                        a.dislikes = a.dislikes + 1
                        a.instantLiked = False
                        a.save()

            except Rating.DoesNotExist:
                #si no existe, lo creo
                myuser = UserMisCosas.objects.get(username=username)
                myuser.itemsvoted = myuser.itemsvoted + 1
                myuser.save()

                if feedType == "yt":
                    a = Video.objects.get(identificador=id2)
                if feedType == "wk":
                    a = Revision.objects.get(identificador=id2)

                a.punctuation = a.punctuation - 1
                a.dislikes = a.dislikes + 1
                a.instantLiked = False
                a.puntuated = True
                a.save()

                title = a.title
                link = a.link
                rating = Rating(punctuation="Like", user=user, username=username,
                        identificador=id2, feedType=feedType, title=title, link=link)
                rating.save()

        elif action == "Enviar Comentario":
            title = request.POST['title']
            link = request.POST['link']
            #guardo el comentario del usuario en la base de datos (con el id del item)
            user = request.user
            username = user.username
            content = request.POST['comment']
            date = str(datetime.datetime.now())
            comment = Comment(content=content, date=date, user=user, username=username,
                        identificador=id2, title=title, link=link)
            comment.save()

            myuser = UserMisCosas.objects.get(username=username)
            myuser.itemscommented = myuser.itemscommented + 1
            myuser.save()


    if request.user.is_authenticated:
        username = request.user.username
        user = UserMisCosas.objects.get(username=username)
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    if feedType == 'yt':
        #saco item Video
        b = Video.objects.get(identificador=id2)
        #print("instantLiked:" + str(b.instantLiked))
        c = Comment.objects.filter(identificador=id2)

        d = Feed.objects.all()
        urls = []
        for feed in d:
            urls.append(feed.link)

        [background, color, size] = change_style(request)

        #busco si hay una puntuación a ese item por ese user
        try:
            punctuation = Rating.objects.get(user=request.user, identificador=id2)
            votedByUser = True
        except:
            votedByUser = False

        context = {'authenticated': authenticated, 'message': message, 'comments': c,
                    'feedType': feedType,'feed': a, 'item': b, 'urls': urls, 'feed_list': d,
                    'background': background, 'color': color, 'size': size, 'votedByUser': votedByUser}
        return render(request, 'miscosas/itempageYt.html', context)

    elif feedType == 'wk':
        #saco item Revision
        b = Revision.objects.get(identificador=id2)
        c = Comment.objects.filter(identificador=id2)

        d = Feed.objects.all()
        urls = []
        for feed in d:
            urls.append(feed.link)

        [background, color, size] = change_style(request)

        #busco si hay una puntuación a ese item por ese user
        try:
            punctuation = Rating.objects.get(user=request.user, identificador=id2)
            votedByUser = True
        except:
            votedByUser = False

        context = {'authenticated': authenticated, 'message': message, 'comments': c,
                    'feedType': feedType, 'feed': a, 'item': b, 'urls': urls, 'feed_list': d,
                    'background': background, 'color': color, 'size': size, 'votedByUser': votedByUser}
        return render(request, 'miscosas/itempageWk.html', context)
    else:
        b = ""


def infopage(request):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            username = request.POST['username']
            password = request.POST['password']
            [authenticated, message] = loguear(request, username, password)

            [background, color, size] = change_style(request)

            context = {'authenticated': authenticated, 'message': message, 'infopage': True,
                        'background': background, 'color': color, 'size': size}
            return render (request, 'miscosas/infopage.html', context)

    if request.user.is_authenticated:
        username = request.user.username
        user = UserMisCosas.objects.get(username=username)
        authenticated = True
        message = "Hola, " + user.username + ". Click aquí para salir:"
    else:
        authenticated = False
        message = "Sesión de invitado. Registrarse:"

    [background, color, size] = change_style(request)

    feed_list = Feed.objects.all()

    context = {'authenticated': authenticated, 'message': message, 'infopage': True,
                'background': background, 'color': color, 'size': size, 'feed_list': feed_list}
    return render (request, 'miscosas/infopage.html', context)

def logoutpage(request):
    logout(request)
    return redirect("/")

def xmlHomepage (request):
    feed_list = Feed.objects.all()
    last5items = []
    if request.user.is_authenticated:
        authenticated = True
        username = request.user.username
        #últimos 5 items votados por el usuario
        #primero saco los id del objeto Rating
        last5ratings = []
        voted_by_user = Rating.objects.filter(user=request.user)
        if len(voted_by_user) <= 5:
            last5ratings = voted_by_user
        else:
            length = len(voted_by_user)
            last5ratings = voted_by_user[(length-5):(length)]

        #una vez tengo la lista con los objetos, saco el id con un for
        #y me guardo en una lista los objetos asociados
        last5items = []
        for rating in last5ratings:
            id = rating.identificador
            feedType = rating.feedType
            if feedType == "yt":
                video = Video.objects.get(identificador=id)
                last5items.append(video)
            if feedType == "wk":
                revision = Revision.objects.get(identificador=id)
                last5items.append(revision)
    else:
        authenticated = False

    a_puntuated = Video.objects.filter(puntuated=True)
    c_puntuated = Revision.objects.filter(puntuated=True)
    top10 = sorted(chain(a_puntuated, c_puntuated), key=attrgetter('punctuation'), reverse=True)[0:9]

    context = {'authenticated': authenticated, 'feed_list': feed_list,
                'top10': top10, 'last5items': last5items}

    return render(request, 'miscosas/homepage.xml', context, content_type="text/xml")

def jsonHomepage (request):
    feed_list = Feed.objects.all()
    last5items = []
    if request.user.is_authenticated:
        authenticated = True
        username = request.user.username
        #últimos 5 items votados por el usuario
        #primero saco los id del objeto Rating
        last5ratings = []
        voted_by_user = Rating.objects.filter(user=request.user)
        if len(voted_by_user) <= 5:
            last5ratings = voted_by_user
        else:
            length = len(voted_by_user)
            last5ratings = voted_by_user[(length-5):(length)]

        #una vez tengo la lista con los objetos, saco el id con un for
        #y me guardo en una lista los objetos asociados
        last5items = []
        for rating in last5ratings:
            id = rating.identificador
            feedType = rating.feedType
            if feedType == "yt":
                video = Video.objects.get(identificador=id)
                last5items.append(video)
            if feedType == "wk":
                revision = Revision.objects.get(identificador=id)
                last5items.append(revision)
    else:
        authenticated = False

    a_puntuated = Video.objects.filter(puntuated=True)
    c_puntuated = Revision.objects.filter(puntuated=True)
    top10 = sorted(chain(a_puntuated, c_puntuated), key=attrgetter('punctuation'), reverse=True)[0:9]

    context = {'authenticated': authenticated, 'feed_list': feed_list,
                'top10': top10, 'last5items': last5items}
    return render(request, 'miscosas/homepage.json', context, content_type="text/json")

def xmlUserpage(request, id):
    itemsvoted = Rating.objects.filter(username=id)
    itemscommented = Comment.objects.filter(username=id)

    context = {'itemsvoted': itemsvoted, 'itemscommented': itemscommented}

    return render(request, 'miscosas/userpage.xml', context, content_type="text/xml")

def jsonUserpage(request, id):
    itemsvoted = Rating.objects.filter(username=id)
    itemscommented = Comment.objects.filter(username=id)

    context = {'itemsvoted': itemsvoted, 'itemscommented': itemscommented}

    return render(request, 'miscosas/userpage.json', context, content_type="text/json")

def xmlUserspage(request):
    users = UserMisCosas.objects.all()

    context = {'users': users}
    return render(request, 'miscosas/userspage.xml', context, content_type="text/xml")

def jsonUserspage(request):
    users = UserMisCosas.objects.all()

    context = {'users': users}
    return render(request, 'miscosas/userspage.xml', context, content_type="text/json")


def xmlItempage(request, id1, id2):
    feed = Feed.objects.get(title=id1)

    if feed.type == "yt":
        item = Video.objects.get(identificador=id2)
    if feed.type == "wk":
        item = Revision.objects.get(identificador=id2)

    comments = Comment.objects.filter(identificador=id2)

    context = {'feed': feed, 'item': item, 'comments': comments, 'feedType': feed.type}
    return render(request, 'miscosas/itempage.xml', context, content_type="text/xml")

def jsonItempage(request, id1, id2):
    feed = Feed.objects.get(title=id1)

    if feed.type == "yt":
        item = Video.objects.get(identificador=id2)
    if feed.type == "wk":
        item = Revision.objects.get(identificador=id2)

    comments = Comment.objects.filter(identificador=id2)

    context = {'feed': feed, 'item': item, 'comments': comments, 'feedType': feed.type}
    return render(request, 'miscosas/itempage.json', context, content_type="text/json")

def xmlFeedpage(request, id):
    #datos del alimentador, lista de items del alimentador
    feed = Feed.objects.get(title=id)

    if feed.type == "yt":
        items = Video.objects.all()
    if feed.type == "wk":
        items = Revision.objects.all()

    context = {'feed': feed, 'items': items}
    return render(request, 'miscosas/feedpage.xml', context, content_type="text/xml")

def jsonFeedpage(request, id):
    #datos del alimentador, lista de items del alimentador
    feed = Feed.objects.get(title=id)

    if feed.type == "yt":
        items = Video.objects.all()
    if feed.type == "wk":
        items = Revision.objects.all()

    context = {'feed': feed, 'items': items}
    return render(request, 'miscosas/feedpage.json', context, content_type="text/json")

def xmlFeedspage(request):
    feeds = Feed.objects.all()

    context = {'feeds': feeds}
    return render(request, 'miscosas/feedspage.xml', context, content_type="text/xml")

def jsonFeedspage(request):
    feeds = Feed.objects.all()

    context = {'feeds': feeds}
    return render(request, 'miscosas/feedspage.json', context, content_type="text/json")
