from django.db import models
from django.contrib.auth.models import User

class Feed (models.Model):
    title = models.TextField()
    link = models.TextField()
    type = models.TextField() #"yt" o "wk"
    selected = models.BooleanField(default = True)

class Video (models.Model):
    title = models.TextField()
    identificador = models.IntegerField()
    link = models.TextField()
    description = models.TextField()
    videoId = models.TextField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    punctuation = models.IntegerField(default=0)
    instantLiked = models.BooleanField(default=False)
    puntuated = models.BooleanField(default=False)
    feedType = models.TextField(default="yt")
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE, null=True)

class Revision (models.Model):
    title = models.TextField()
    identificador = models.IntegerField()
    link = models.TextField()
    author = models.TextField()
    date = models.TextField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    punctuation = models.IntegerField(default=0)
    instantLiked = models.BooleanField(default=False)
    puntuated = models.BooleanField(default=False)
    feedType = models.TextField(default="wk")
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE, null=True)

class UserMisCosas (models.Model):
    #asumiendo que el username es también el id
    username = models.TextField()
    photo = models.TextField(default = "https://pbs.twimg.com/profile_images/1358879756/default_user_icon_bigger_400x400.png")
    itemsvoted = models.IntegerField(default=0)
    itemscommented = models.IntegerField(default=0)
    style = models.TextField(default="Ligero")
    size = models.TextField(default="Normal")


#la clase comment es un único comentario
class Comment (models.Model):
    content = models.CharField(max_length=256)
    date = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    username = models.TextField()
    identificador = models.IntegerField()
    title = models.TextField()
    link = models.TextField()


class Rating (models.Model):
    punctuation = models.TextField()
    #si se encuentra una puntuación del user y para ese item, se mirará cuál es
    #y si es distinta, se cambiará (si es la misma, se ignorará)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    username = models.TextField()
    feedType = models.TextField()
    identificador = models.IntegerField()
    title = models.TextField()
    link = models.TextField()
