
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

from .models import Video


class YTHandler(ContentHandler):

    def __init__ (self):

        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""
        #nuevos
        self.channel = ""
        self.urlChannel = ""
        self.description = ""
        self.videoId = ""

        self.videos = []
        self.canal = []


    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == "media:description":
                self.inContent = True
            elif name == "yt:videoId":
                self.inContent = True
            #url del canal
            elif name == "uri":
                self.inContent = True
            #nombre del canal
            elif name == "name":
                self.inContent = True


    def endElement (self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'description': self.description,
                                'videoId': self.videoId})
            #print(self.videos)
            self.canal = {'nombreCanal': self.channel,
                            'urlCanal': self.urlChannel}



        elif self.inEntry:
            if name == 'title':
                self.title = self.content
            elif name == "media:description":
                self.description = self.content
            elif name == "yt:videoId":
                self.videoId = self.content
            elif name == "uri":
                self.urlChannel = self.content
            elif name == "name":
                self.channel = self.content

            #lo pongo al final porque aparecía en todas
            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


class YTChannel:
    #esto es siempre igual
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):
        return self.handler.videos

    def canal (self):
        return self.handler.canal


# Load parser and driver
#Parser = make_parser()
#Parser.setContentHandler(YTHandler())

#if __name__ == "__main__":

    #if len(sys.argv)<2:
        #print("Usage: python xml-parser-youtube.py <document>")
        #print()
        #print(" <document>: file name of the document to parse")
        #sys.exit(1)

    # Ready, set, go!
    #xmlFile = open("https://www.youtube.com/feeds/videos.xml?channel_id=" + myID)

    #Parser.parse(xmlFile)
