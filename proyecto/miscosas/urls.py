
from django.urls import path, include
from django.conf.urls import url, include

from miscosas import views

urlpatterns = [
    path('', views.homepage),
    path('format=xml/', views.xmlHomepage),
    path('format=json/', views.jsonHomepage),

    path('users/', views.userspage),
    path('users/format=xml/', views.xmlUserspage),
    path('users/format=json/', views.jsonUserspage),

    path('users/<str:id>/', views.userpage),
    path('users/<str:id>/format=xml/', views.xmlUserpage),
    path('users/<str:id>/format=json/', views.jsonUserpage),

    path('feeds/', views.feedspage),
    path('feeds/format=xml/', views.xmlFeedspage),
    path('feeds/format=json/', views.jsonFeedspage),

    path('feeds/<str:id>/', views.feedpage),
    path('feeds/<str:id>/format=xml/', views.xmlFeedpage),
    path('feeds/<str:id>/format=json/', views.jsonFeedpage),

    path('feeds/<str:id1>/<str:id2>/', views.itempage),
    path('feeds/<str:id1>/<str:id2>/format=xml/', views.xmlItempage),
    path('feeds/<str:id1>/<str:id2>/format=json/', views.jsonItempage),

    path('info/', views.infopage),
    path('logout/', views.logoutpage),

    url(r'^i18n/', include('django.conf.urls.i18n')),
]
